﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

namespace KS_NovelScriptCompiler
{
    public enum EType
    {
        adr,
        num,
        str,
        reg,
        any,
        _Num
    }

    //命令のデータ
    public class Opr
    {
        public int oprID { get; set; } = 0;
        public List<EType> args_type { get; set; } = new List<EType>();
        public List<string> args { get; set; } = new List<string>();
    }

    //命令のフォーマット
    public class OprFormat
    {
        public int ID { get; set; } = 0;
        public string Name { get; set; } = "";
        public List<EType> args_type { get; set; } = new List<EType>();
    }

    //コンパイル処理を行うクラス
    public class Compiler
    {
        #region private member
        private DebugIO _Debug = null;

        //ソースコード
        private string _Source = "";

        //ソースコードのファイルパス
        private string _SourcePath = "";

        //ベースのディレクトリパス
        private string _BaseDirPath { get; set; } = "";

        //ラベルリスト
        private Dictionary<string,int> _LabelList = new Dictionary<string, int>();

        //includeしたファイル一覧
        private List<string> _IncludedFiles = new List<string>();

        //define一覧
        private Dictionary<string, string> _Defines = new Dictionary<string, string>();

        //アセンブリコードの格納バッファ
        private List<Opr> _ProgramBuffer = new List<Opr>();

        //ファイル操作系のユーティリティ
        private Utility _Utils = null;

        //命令セット
        private List<OprFormat> _OprSets { get; set; } = new List<OprFormat>();
        #endregion

        Compiler() { }
        public Compiler(string path, DebugIO d) { _SourcePath = path; _Debug = d; _Utils = new Utility(_Debug); }

        //コンパイル実行
        public void Compile()
        {
            _BaseDirPath = Path.GetDirectoryName(_SourcePath) +"/";
            _Source = _Utils.LoadStringFromFile(_SourcePath);

            //命令セットをロード
            _Debug.Log("[info]命令セットのロードを開始。");
            LoadOprSets();
            _Debug.Log("[info]命令セットのロード完了。");

            //自身を誤ってincludeしないようにリストへ追加しておく
            _IncludedFiles.Add(_SourcePath);

            //コンパイラ命令処理
            _Source = CompilerOprProcess(_Source);

            //行ごとに分解
            var ColumnList = _Source.Split('\n').ToList();

            //空行を削除
            ColumnList.RemoveAll((string s) => { return s == ""; });

            //ラベルの事前捜査（PCは確定していないので、名前だけ探査する
            _Debug.Log("[info]ラベルの事前探査開始");
            PreAnalyzeLabel(ColumnList);
            _Debug.Log("[info]ラベルの事前探査完了");

            //字句解析・構文解析
            _Debug.Log("[info]字句解析、構文解析開始");
            AnalyzeProgram(ColumnList);
            _Debug.Log("[info]字句解析、構文解析完了");

            //ラベル解析・サブルーチン置換(意味解析)
            _Debug.Log("[info]ラベルの対応付け開始");
            AnalyzeLabel();
            _Debug.Log("[info]ラベルの対応付け完了");

            //出力
            //Buildフォルダに同名のcsv形式で書きだす。
            var outputPath = Path.GetDirectoryName(_SourcePath) + "/Build/" + Path.GetFileNameWithoutExtension(_SourcePath) + ".csv";
            OutputAssembly(outputPath);
            _Debug.Log("[info]ファイルに出力完了：" + outputPath);
        }

        #region コンパイラ命令処理

        //コンパイラ命令処理
        private string CompilerOprProcess(string source)
        {
            var outstr = source;

            //コメント削除
            outstr = DeleteComment(outstr);
            //include 処理
            _Debug.Log("[info]include処理開始");
            outstr = IncludeAllSource(outstr,_BaseDirPath);
            _Debug.Log("[info]include処理完了");
            //キャリッジリターン置換
            outstr = outstr.Replace("\r", "");
            //define分置換
            _Debug.Log("[info]define文処理開始");
            outstr = ReplaceDefine(outstr);
            _Debug.Log("[info]define文処理完了");

            return outstr;
        }

        //includeルーチン
        private string IncludeAllSource(string source,string basePath)
        {
            string outStr = source;

            //include文を一括で検索
            var matches = Regex.Matches(outStr, " *[*] *include *\".+\" *(\r|)\n");

            //見つかったものをすべて置換する
            foreach (Match match in matches)
            {
                _Debug.Log("【info】次のファイルをincludeしました：" + match.Value);

                //一致したテキストはファイル名を読み出してテキストを挿入する。
                var filepath = Regex.Match(match.Value, "\".+\"").Value;
                filepath = filepath.Replace("\"", ""); // "は除く
                filepath = basePath + filepath;

                //ファイルリストにファイルを追加
                //すでにある場合はスキップする(onceの処理)
                if (_IncludedFiles.Contains(filepath))
                {
                    outStr = outStr.Replace(match.Value, "");
                    continue;
                }
                _IncludedFiles.Add(filepath);

                //テキストを読み出してコメントは消しておく。
                var insertStr = _Utils.LoadStringFromFile(filepath);
                insertStr = DeleteComment(insertStr);

                //読み出したものに対してもinclude文があれば置換するよう、再帰的に呼び出す。
                insertStr = IncludeAllSource(insertStr, Path.GetDirectoryName(filepath) + "/");

                //読み出したテキストでinclude文を置換する
                outStr = outStr.Replace(match.Value, "");
                outStr = outStr.Insert(match.Index, insertStr);

            }

            return outStr;
        }

        //コメント削除ルーチン
        private string DeleteComment(string source)
        {
            var outText = source;

            //コメントのフォーマットをマッチングして消去
            Regex reg = new Regex("<!--.*?-->",RegexOptions.Singleline);
            outText = reg.Replace(outText, "");

            return outText;
        }

        //define置換ルーチン
        private string ReplaceDefine(string source)
        {
            string outStr = source;

            var matches = Regex.Matches(outStr, " *[*] *define *.+(\r|)\n");
            foreach (Match match in matches)
            {
                _Debug.Log("【info】次のdefineを置換しました：" + match.Value);

                //一致したテキストはファイル名を読み出してテキストを挿入する。
                var str = Regex.Replace(match.Value, " *[*] *define *", "");
                str = str.Replace("\n","");
                var keywords = _Utils.Split(str, ' ').ToList();//スペース区切りで取得
                keywords.RemoveAll((string s) => { return s.Equals(""); }); //空要素を削除

                //キーワードが足りなければ無視する
                if(keywords.Count != 1 && keywords.Count != 2)
                {
                    outStr = outStr.Replace(match.Value, "");
                    _Debug.Log("【Warning】defineのフォーマットがおかしいです。" + match.Value);
                    continue;
                }

                //置換先キーワードが未指定の場合、空文字を入れておく
                if (keywords.Count == 1) keywords.Add("");

                //defineリストにdefineを追加
                //すでにある場合はリストに上書きしておく
                _Defines.Add(keywords[0], keywords[1]);

                //define文自体は消しておく
                outStr = outStr.Replace(match.Value, "");
                
            }

            //全部検出したら一気に置換する
            foreach (var def_pair in _Defines)
            {
                outStr = outStr.Replace(def_pair.Key, def_pair.Value);
            }

            return outStr;
        }

        #endregion

        #region コンパイル処理
        //ラベルの事前解析
        private void PreAnalyzeLabel(List<string> columns)
        {
            foreach(var c in columns)
            {
                var m = Regex.Match(c, "^ *# *[^ ]+ *$", RegexOptions.Compiled);
                if(m.Success)
                {
                    var labelname = m.Value.Replace("#", "").Replace(" ", "");
                    _LabelList.Add(labelname, 0);
                }
            }

            //mainラベルが見つからなかったら、pc-1でmainラベルを入れておく
            if (!_LabelList.ContainsKey("main"))
            {
                _LabelList.Add("main", -1);
            }
        }

        //ラベル解析
        private void AnalyzeLabel()
        {
            //ラベル命令の番号を取得
            var id = _OprSets.Find((x) => { return x.Name == "_label"; }).ID;

            for (int i=0; i<_ProgramBuffer.Count; i++)
            {
                //ラベルを探索
                if(_ProgramBuffer[i].oprID == id)
                {
                    _LabelList[_ProgramBuffer[i].args[0]] = i;
                }
            }
        }

        //アセンブルする
        private void AnalyzeProgram(List<string> columnList)
        {
            foreach(var c in columnList)
            {
                //システム命令の場合
                var m = Regex.Match(c, "^ *> *[^ ]+( +[^ ]+)* *$", RegexOptions.Compiled);
                if(m.Success)
                {
                    //キーワード軍のみを取り出す。
                    var str = m.Value.Replace(">", "");
                    var strlist = _Utils.Split(str, ' ').ToList();
                    strlist.RemoveAll((string s) => { return s == ""; });

                    try
                    {
                        _Debug.Log("【info】[命令]:" + strlist[0]);

                        //命令をリストから探索
                        var opeformats = _OprSets.FindAll((OprFormat op) => { return op.Name == strlist[0]; });
                        if(opeformats.Count==0)
                        {
                            //命令リストになければラベルを探索
                            if(_LabelList.ContainsKey(strlist[0]))
                            {
                                var labelname = new List<string>();
                                labelname.Add(strlist[0]);

                                //引数の処理
                                var args = new List<string>();
                                args.AddRange(strlist.GetRange(1, strlist.Count - 1));

                                var push_args = new Stack<string>(); //push命令用スタック（逆順にpushするのでstack構造）
                                var pop_args = new List<string>(); //返り値格納用の引数。call命令の後にpop命令として積む

                                // :が出るまではpush命令で先積み
                                int i = 0;
                                for (; i<args.Count; i++)
                                {
                                    // :以降は返り値なので、callの後に挟む。
                                    if (args[i] == ":") { i++; break; }

                                    push_args.Push(args[i]);
                                }
                                // :以降はpop命令で、後積み
                                for(; i<args.Count; i++)
                                {
                                    pop_args.Add(args[i]);
                                }

                                //push命令生成
                                while(push_args.Count > 0)
                                {
                                    var tmp = new List<string>();
                                    tmp.Add(push_args.Pop());
                                    _ProgramBuffer.Add(ConvertOpr("push", tmp));
                                }

                                //ラベルをコール命令に変換して追加
                                _ProgramBuffer.Add(ConvertOpr("call", labelname));

                                //pop命令生成
                                foreach (var a in pop_args)
                                {
                                    var tmp = new List<string>();
                                    tmp.Add(a);
                                    _ProgramBuffer.Add(ConvertOpr("pop", tmp));
                                }
                            }
                            else
                            {
                                //なければエラー
                                throw new IdentifierNotFoundException("[Error]識別子エラーです。" + strlist[0]);
                            }
                            
                        }
                        //リターン命令は別処理
                        //引数は全部スタックに積んでおく
                        else if(strlist[0] == "return")
                        {
                            //引数を取得
                            var args = strlist.Count > 1 ? strlist.GetRange(1, strlist.Count - 1) : new List<string>();

                            //引数は逆順にpushで積んでおく
                            for( int i=args.Count-1; i>= 0; i--)
                            {
                                var tmp = new List<string>();
                                tmp.Add(args[i]);
                                _ProgramBuffer.Add(ConvertOpr("push", tmp));
                            }
                            //リターン命令を追加
                            _ProgramBuffer.Add(ConvertOpr("return", new List<string>()));
                        }
                        else
                        {
                            //命令セットにあれば、その通り命令を出力
                            var args = strlist.Count > 1 ? strlist.GetRange(1, strlist.Count - 1) : new List<string>();
                            _ProgramBuffer.Add(ConvertOpr(strlist[0], args));

                        }
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        throw new ArgumentOutOfRangeException("[Error]システム命令の命令名が記述されていません。");
                    }
                    catch(IdentifierNotFoundException e)
                    {
                        throw;
                    }

                    //引数を読み出す
                    continue;
                }

                //ラベルの場合
                m = Regex.Match(c, "^ *# *[^ ]+ *$", RegexOptions.Compiled);
                if(m.Success)
                {
                    var labelName = m.Value.Replace("#", "").Replace(" ", "");

                    //ラベル命令を発行
                    var opr = new Opr();
                    opr.oprID = _OprSets.Find((x) => { return x.Name == "_label"; }).ID;
                    opr.args_type.Add(EType.str);
                    opr.args.Add(labelName);

                    _ProgramBuffer.Add(opr);
                    continue;
                }

                //ノベル文（名前）の場合
                m = Regex.Match(c, "^ *[-] *[^ ]+ *([[].*[]]|) *$", RegexOptions.Compiled);
                if(m.Success)
                {
                    //名前切り出し
                    var name = m.Value.Replace("-", "").Replace(" ", "");
                    name = Regex.Replace(name, "[[].*[]]","");

                    //名前表示命令に変換して発行
                    var opr = new Opr();
                    opr.oprID = _OprSets.Find((x) => { return x.Name == "_name"; }).ID;
                    opr.args_type.Add(EType.str);
                    opr.args.Add(name);

                    _ProgramBuffer.Add(opr);

                    //ボイス命令があれば
                    //ボイス命令切り出し
                    var m_voice = Regex.Match(m.Value, "[[].*[]]");
                    if (m_voice.Success)
                    {
                        var voice_name = m_voice.Value;
                        voice_name = voice_name.Replace("[", "").Replace("]", ""); //[]を外す

                        opr = new Opr();
                        opr.oprID = _OprSets.Find((x) => { return x.Name == "_voice"; }).ID;
                        opr.args_type.Add(EType.str);
                        opr.args.Add(voice_name);
                        _ProgramBuffer.Add(opr);
                    }

                    continue;
                }

                //ノベル分（平文）の場合
                //テキスト出力命令に変換して発行
                {
                    //改ページ文字を含んでいるかチェックし、置換する
                    var replaced_str = c;
                    var isNewPage = false;
                    if (c.Contains("」"))
                    {
                        isNewPage = true;
                        replaced_str = c.Replace("」", "");
                    }

                    var opr = new Opr();
                    opr.oprID = _OprSets.Find((x) => { return x.Name == "_text"; }).ID;
                    opr.args_type.Add(EType.str);
                    opr.args.Add(replaced_str);

                    _ProgramBuffer.Add(opr);

                    //もし改ページ文字」を含んでいたら改ページ命令を入れる
                    if(isNewPage)
                    {
                        opr = new Opr();
                        opr.oprID = _OprSets.Find(x => { return x.Name == "_newpage"; }).ID;

                        _ProgramBuffer.Add(opr);
                    }
                }
            }

        }

        //命令セットをロード
        private void LoadOprSets()
        {
            var oprSetsRawdata = _Utils.LoadDataFromCSV(AppDomain.CurrentDomain.BaseDirectory + "/OprList.csv");
            foreach (var o in oprSetsRawdata)
            {
                var opr = new OprFormat();
                opr.ID = int.Parse(o[0]);
                opr.Name = o[1];
                foreach (var arg in o.GetRange(2, o.Count - 2))
                {
                    if (arg == "") continue;//空白文字の場合はスキップ

                    //文字列から方オブジェクトへ返還(*の場合はanyを指定)
                    if (arg == "*") opr.args_type.Add(EType.any);
                    else opr.args_type.Add((EType)Enum.Parse(typeof(EType), arg));
                }
                _OprSets.Add(opr);
            }
        }

        //命令コマンドをバイナリに変換して返す
        private Opr ConvertOpr(string ope, List<string> args)
        {
            //命令セットの中から命令を検索する
            var opelists = _OprSets.FindAll((OprFormat f) => { return f.Name == ope;  });
            foreach (var o in opelists)
            {
                //命令セットの引数と現実の引数の数があっているかの判定
                //任意の数の引数の場合もある
                //少ない場合はオーバーロードの可能性があるので、continueする
                if(args.Count != o.args_type.Count)
                {
                    //引数が2より少ない場合は任意の数の引数の可能性はないので、棄却する
                    if (o.args_type.Count > 1)
                    {
                        //引数の数がフォーマット-1以上で、最後の要素が*になっている命令の場合のみ任意の数の引数であり、それ以外は棄却する
                        if (!(args.Count >= o.args_type.Count - 1 && o.args_type.Last() == EType.any))
                            continue;
                    }
                    else
                        continue;
                }

                //データの代入
                var op_dat = new Opr();
                op_dat.oprID = o.ID;

                //引数のチェック
                var args_dat = new Opr();
                bool isSuccess = true;
                for (int i = 0; i < args.Count; i++)
                {
                    //フォーマットから型を取得する
                    EType checkType = EType.num;
                    if(o.args_type.Count-1 < i)
                    {
                        if(o.args_type.Last() == EType.any)
                            checkType = o.args_type[o.args_type.Count - 2];
                        else
                        {
                            isSuccess = false;
                            break;
                        }
                    }
                    else if(o.args_type.Count-1 == i)
                    {
                        if(o.args_type[i] == EType.any)
                            checkType = o.args_type[o.args_type.Count - 2];
                        else
                            checkType = o.args_type[i];

                    }
                    else
                    {
                        checkType = o.args_type[i];
                    }

                    //型チェック(形ちがいのオーバーロードの可能性があるので型チェックする
                    //ただし、reg型のみ、num,strの代用としてチェックを通過するものとする
                    var argsType = CheckStrType(args[i]);
                    if (checkType == argsType ||
                        ((checkType == EType.num || checkType == EType.str) && argsType == EType.reg))
                    {
                        args_dat.args_type.Add(argsType);
                        args_dat.args.Add(args[i]);
                    }
                    else
                    {
                        isSuccess = false;
                        break;
                    }
                }

                //引数判定が正しく行われた場合は合格とし、値を格納する
                if (isSuccess)
                {
                    op_dat.args_type = args_dat.args_type;
                    op_dat.args = args_dat.args;
                    return op_dat;
                }
            }

            //エラーメッセージを構成
            var error_mes = "[oprcode]" + ope;
            foreach (var a in args) error_mes += " [args]" + a;

            throw new IdentifierNotFoundException("[Error]命令が見つかりません。命令フォーマットを確認してください。" + error_mes);
        }

        //文字列の肩を判定する
        private EType CheckStrType(string str)
        {
            bool isLebel = _LabelList.ContainsKey(str);
            
            if (Regex.Match(str, "^\".*\"$").Success) return EType.str;
            else if (Regex.Match(str, "^@[a-z]*[0-9]*$").Success) return EType.reg;
            else if (Regex.Match(str, "^(|-)[0-9]+(|[.])[0-9]*$").Success) return EType.num;       //0-9で構成された文字を数とする
            else if (isLebel) return EType.adr;
            else
            {
                throw new IdentifierNotFoundException("[Error]" + str + "：識別子が不明です。");
            }
        }
        #endregion

        //アセンブリをファイルに書き込む処理（CSV形式)
        private void OutputAssembly(string path)
        {
            //保存形式にプログラムを整理する
            //ラベルコードを追加
            var outstr = "";
            outstr += "label:\r\n";
            foreach(var l in _LabelList)
            {
                outstr += l.Key + "," + l.Value.ToString() + "\r\n";
            }

            //プログラムコードを追加
            outstr += "prog:\r\n";
            foreach(var p in _ProgramBuffer)
            {
                outstr += p.oprID.ToString() + ",";
                for(int i=0; i<p.args.Count; i++)
                {
                    outstr += (p.args_type[i]).ToString() + ",";
                    outstr += p.args[i] + ",";
                }
                outstr += "\r\n";
            }

            //ファイルにセーブ
            _Utils.SaveToFile(path, outstr);
        }

    }
}
