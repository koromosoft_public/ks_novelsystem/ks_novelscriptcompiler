﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_NovelScriptCompiler
{
    public class DebugIO
    {
        public string LogText { get; private set;}

        public void Log(string str)
        {
            System.Diagnostics.Debug.WriteLine(str);
            System.Console.WriteLine(str);
            LogText += str + "\r\n";
        }
    }
}
