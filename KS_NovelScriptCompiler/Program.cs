﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_NovelScriptCompiler
{
    class Program
    {
        static void Main(string[] args)
        {
            var debug = new DebugIO();
            var utils = new Utility(debug);

            if (args.Length == 0)
            {
                debug.Log("[Error]引数にコンパイルするファイル名を指定してください。");
                return;
            }

            //各ファイルを一括コンパイル
            foreach (var file in args)
            {
                var compiler = new Compiler(file, debug);
                debug.Log(Path.GetFileName(file) + ":コンパイルを開始しました。");
                try
                {
                    compiler.Compile();
                }
                catch(Exception e)
                {
                    debug.Log(e.Message);
                }
                finally
                {
                    utils.SaveLog();
                }
                debug.Log(Path.GetFileName(file) + ":コンパイル終了。");
            }

            //ログを出力
            utils.SaveLog();
        }
    }
}
