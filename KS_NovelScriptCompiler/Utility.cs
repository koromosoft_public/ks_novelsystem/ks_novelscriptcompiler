﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace KS_NovelScriptCompiler
{
    public class Utility
    {
        DebugIO _Debug = null;

        Utility() { }
        public Utility(DebugIO d) { _Debug = d; }

        public List<List<string>> LoadDataFromCSV(string filepath)
        {
            var outList = new List<List<string>>();

            var str = LoadStringFromFile(filepath);
            str = str.Replace("\r", "");

            //行を読み出し
            var colmuns = str.Split('\n');
            foreach (var c in colmuns)
            {
                if (c == "") continue;

                //列を読み出し
                var rows = c.Split(',');
                outList.Add(new List<string>());
                foreach (var r in rows)
                {
                    //データを格納
                    outList.Last().Add(r);
                }
            }

            return outList;
        }

        public string LoadStringFromFile(string FilePath)
        {
            var outStr = "";

            try
            {
                using (FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (StreamReader sr = new StreamReader(fs, Encoding.UTF8))
                    {
                        outStr = sr.ReadToEnd();
                    }
                }
            }
            catch (FileNotFoundException)
            {
                _Debug.Log("[Error]ファイルが見つかりません。" + FilePath);
                Environment.Exit(0);
            }
            catch (ArgumentException)
            {
                _Debug.Log("[Error]ファイル名が空の可能性があります。" + FilePath);
            }

            return outStr;
        }

        public void SaveToFile(string FilePath, string dat)
        { 
            try
            {
                var dirPath = Path.GetDirectoryName(FilePath);
                //ディレクトリがなければ作成
                if (dirPath != "")
                {
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }
                }

                StreamWriter sw = new StreamWriter(FilePath, false, Encoding.GetEncoding("UTF-8"));
                sw.Write(dat);
                sw.Close();
            }
            catch(DirectoryNotFoundException)
            {
                _Debug.Log("[Error]ディレクトリが見つかりません。ファイルパスを確認してください。" + FilePath);
            }
            catch(UnauthorizedAccessException)
            {
                _Debug.Log("[Error]保存対象のファイルにアクセスできません。ほかのプロセスが使用していないか、権限が十分か確認してください。" + FilePath);
            }
            catch(Exception)
            {
                _Debug.Log("[Error]不明なエラーです。開発者に連絡してください。" + FilePath);
            }
        }

        //ログをファイルに出力
        public void SaveLog()
        {
            SaveToFile("log.txt", _Debug.LogText);
        }

        //文字列を考慮したスプリット
        public string[] Split(string source, char splitter)
        {
            var strbuf = new List<string>(); //文字列として検出したものを一時的にためておくバッファ
            var matches = Regex.Matches(source, "\"[^\"]*\"");

            //すべての文字列を一時的に"に変換しておく
            var tmpstr = source;
            tmpstr = Regex.Replace(tmpstr, "\"[^\"]*\"", "\"");

            //スプリッターでリスト化
            var list = tmpstr.Split(splitter);

            var index = 0;
            for(var i=0; i<list.Length; i++)
            {
                //もし文字列だった場合は置換する
                if (list[i] == "\"")
                {
                    try
                    {
                        list[i] = matches[index++].Value;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        _Debug.Log("[Error]文字列が閉じられていません。:" + source);
                    }
                }
            }
            return list;
        }
    }
}

//識別子が見つからないエラー用
public class IdentifierNotFoundException : Exception
{
    public IdentifierNotFoundException()
    {

    }

    public IdentifierNotFoundException(string message) : base(message)
    {

    }

    public IdentifierNotFoundException(string message, Exception e) : base(message,e)
    {

    }
}